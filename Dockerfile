FROM ubuntu:latest
RUN apt-get update && apt-get install -y pgtop && apt-get clean && rm -rf /var/lib/apt/lists/*
CMD pg_top
ENTRYPOINT ["/usr/bin/pg_top"]
