# docker pgtop

https://hub.docker.com/r/labeme/pgtop/

https://gitlab.com/labe-me/docker-pgtop

The pg_top tool inside a container.

    docker run -it --rm labeme/pgtop --help

You may have to attach to some network to join your database container:

    docker run --rm -it --net mydbnet labeme/pgtop --help
